# dungeonsagaorigins



## What's the deal?
  
I'm making bits and pieces for my kickstarter copy of Mantic Games' Dungeon Saga Origins.  
  
I'll be starting with some openSCAD files and 3D STLs, but might do other stuff later.


<a href="cclicense-by-nc-sa.md">License - CC4.0 by-nc-sa</a> aka copy and change and share if you want, but make sure you give me credit, and don't sell anything you make off the back of this project.