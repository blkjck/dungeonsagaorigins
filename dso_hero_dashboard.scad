// Variable length measuring widget with ticks all along at x increments, with a labelled circular thumb spot for holing it.

// variables 

//hero card
ot = 1.2;       // object thickness in units as per scl. do multiples of 0.2mm for best layer compatibility
ox = 126 ;      // object x length in units as per scl
oy = 88;        // object y length in units as per scl
bw = 5;         // boundary width - how much bigger than the card should the dashboard be
ov = 3;         // overlap of frame over card
dh = 0.3;       // detail height  do multiples of 0.2mm for best layer compatibility
br = 8;         // boon token hole radius. token actually 15mm diameter
rr = 16;        // revive token hole radius. token actually 25mm diameter

//feat card
fx = 62;        //feat card x size
fy = 88;        //feat card y size

//TEXT SETTINGS
fs = 100;       //font size
ff = "Roboto:style=Bold"; // font's font and style
res = 360;      //resolution of curves. higher means more smooth


//calculated
hs = bw+ov ;    //hole size border
hsx = bw+ox-25-ov; //hole size x direction from origin
pr = bw/3;      //post radius

// begin dashboard

difference()
{
// top layer 
    // main body
    linear_extrude(height = ot ) polygon(points=[[0,0],[ox+2*bw,0],[ox+2*bw,oy+2*bw],[0,oy+2*bw] ]);
    
    //cut the main slot out
    linear_extrude(height = ot ) polygon(points=[[hs,hs],[hsx,hs],[hsx,oy+bw-ov],[hs,oy+bw-ov] ]);
    
    //cut out heart slots
    linear_extrude(height = ot ) polygon(points=[[115,hs],[125,hs+10],[115,hs+20],[105,hs+10] ]);
translate([0,18,0])
    linear_extrude(height = ot ) polygon(points=[[115,hs],[125,hs+10],[115,hs+20],[105,hs+10] ]);
translate([0,33,0])
    linear_extrude(height = ot ) polygon(points=[[115,hs],[125,hs+10],[115,hs+20],[105,hs+10] ]);
translate([0,48,0])
    linear_extrude(height = ot ) polygon(points=[[115,hs],[125,hs+10],[115,hs+20],[105,hs+10] ]);
translate([0,63,0])
    linear_extrude(height = ot ) polygon(points=[[115,hs],[125,hs+10],[115,hs+20],[105,hs+10] ]);

// alignment posts
    translate([bw/2,bw/2,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([ox+1.5*bw,bw/2,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([bw/2,oy+1.5*bw,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([ox+1.5*bw,oy+1.5*bw,0])
    cylinder(h=ot,r=pr,$fn=res);
}

translate([0,-oy-2*bw-10,0])

union()
{
  // alignment posts
    translate([bw/2,bw/2,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([ox+1.5*bw,bw/2,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([bw/2,oy+1.5*bw,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([ox+1.5*bw,oy+1.5*bw,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
   
difference()
{// BOTTOM layer 
    // main body
    linear_extrude(height = ot ) polygon(points=[[0,0],[ox+2*bw,0],[ox+2*bw,oy+2*bw],[0,oy+2*bw] ]);
    
    // card slot
    translate([0,0,ot/2])
    linear_extrude(height = ot/2 ) polygon(points=[[bw-1,bw-1],[ox+bw+1,bw-1],[ox+bw+1,oy+2*bw],[bw-1,oy+2*bw] ]);
    
    //label
    translate([bw+ox/2,bw+oy/2,ot/2-dh])    
    linear_extrude(height = dh )
    text("HERO", valign="center", halign="center", $fn=res);
 } }


// begin feat dashboard

translate([2*bw+ox,0,0])
difference()
{
// top layer 
    // main body
    linear_extrude(height = ot ) polygon(points=[[0,0],[fx+2*bw,0],[fx+2*bw,fy+2*bw],[0,fy+2*bw] ]);
    
    //cut the main slot out
    linear_extrude(height = ot ) polygon(points=[[hs,hs],[fx+bw-ov,hs],[fx+bw-ov,fy+bw-ov],[hs,fy+bw-ov] ]);
    
    
// alignment posts
    translate([bw/2,bw/2,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([fx+1.5*bw,bw/2,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([bw/2,fy+1.5*bw,0])
    cylinder(h=ot,r=pr,$fn=res);
    
    translate([fx+1.5*bw,fy+1.5*bw,0])
    cylinder(h=ot,r=pr,$fn=res);
}

translate([2*bw+ox,-oy-2*bw-10,0])

union()
{
  // alignment posts
    translate([bw/2,bw/2,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([fx+1.5*bw,bw/2,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([bw/2,fy+1.5*bw,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
    
    translate([fx+1.5*bw,fy+1.5*bw,0])
    cylinder(h=2*ot,r=0.9*pr,$fn=res);
   
difference()
{// BOTTOM layer 
    // main body
    linear_extrude(height = ot ) polygon(points=[[0,0],[fx+2*bw,0],[fx+2*bw,fy+2*bw],[0,fy+2*bw] ]);
    
    // card slot
    translate([0,0,ot/2])
    linear_extrude(height = ot/2 ) polygon(points=[[bw-1,bw-1],[fx+bw+1,bw-1],[fx+bw+1,fy+2*bw],[bw-1,fy+2*bw] ]);
    
    //label
    translate([bw+fx/2,bw+fy/2,ot/2-dh])    
    linear_extrude(height = dh )
    text("FEAT", valign="center", halign="center", $fn=res);
 } }